<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $formOffice yii\bootstrap\ActiveForm */
/* @var $formPhone yii\bootstrap\ActiveForm */
/* @var $modelOffice app\models\ConsultationOffice */
/* @var $modelPhone app\models\ConsultationPhone */

$this->title = 'Главная';
$listOptions = ['class' => 'form-control', 'prompt' => '- выбрать -'];

Modal::begin([
    'header' => '<h2>Консультация онлайн</h2>',
    'toggleButton' => ['label' => 'Консультация онлайн', 'class' => 'btn btn-success'],
]);

echo Html::textInput('text', '', ['class' => 'form-control']);

Modal::end();
?>
<div class="site-index">
    
    <div class="row">
    
        <div class="col-lg-6">
            
            <h1>Запись на консультацию в офис</h1>
        
            <?php $formOffice = ActiveForm::begin(['layout' => 'horizontal']); ?>
            
            <?= $formOffice->field($modelOffice, 'name')->textInput(['maxlength' => true]) ?>

            <?= $formOffice->field($modelOffice, 'city')->textInput(['maxlength' => true]) ?>

            <?= $formOffice->field($modelOffice, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '999-999-9999']) ?>
    
            <?= $formOffice->field($modelOffice, 'time')->dropDownList($modelOffice->consultation_time, $listOptions) ?>
    
            <?= $formOffice->field($modelOffice, 'day')->dropDownList($modelOffice->consultation_day, $listOptions) ?>
    
            <?= $formOffice->field($modelOffice, 'text')->textArea(['rows' => '6']) ?>
            
            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
            </div>
            
            <?php ActiveForm::end(); ?>
        
        </div>
        
        <div class="col-lg-6">
            
            <h1>Запись на консультацию по телефону</h1>
        
            <?php $formPhone = ActiveForm::begin(['layout' => 'horizontal']); ?>
            
            <?= $formPhone->field($modelPhone, 'name')->textInput(['maxlength' => true]) ?>

            <?= $formPhone->field($modelPhone, 'city')->textInput(['maxlength' => true]) ?>

            <?= $formPhone->field($modelPhone, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '999-999-9999']) ?>
            
            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
            </div>
            
            <?php ActiveForm::end(); ?>
        
        </div>
    
    </div>
    
</div>
<?php
namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\FileHelper;
use yii\imagine\Image;

class SiteHelper
{
    /**
     * Вывод фильтра и значения в колонке "Активно" GridView
     * @param object $searchModel
     * @return array
     */
    public static function is_active($searchModel)
    {
        return [
            'attribute' => 'is_active',
            'filter' => Html::activeDropDownList(
            $searchModel,
            'is_active',
            [0 => 'Не активно', 1 => 'Активно'],
                ['class' => 'form-control', 'prompt' => '- выбрать -']
            ),
            'value' => function ($model, $index, $widget) {
                return $model->is_active == 1 ? 'Активно' : 'Не активно';}
        ];
    }
    
    /**
     * Ресайз изображения
     * @param string $image изображение
     * @param int $width ширина
     * @param mixed $height высота  
     * @return string Url изображения
     */
    public static function resized_image($image = '', $width, $height = '')
    {
        if (!empty($image)) {
            $file = explode('/', $image);
            $file = end($file);
            $dir = Yii::getAlias('@webroot/images/uploads/') . $width . 'x' . $height;
            $img = $dir . '/' . $file;
            
            if (file_exists($img)) {
                $url = true;
            } else {
                FileHelper::createDirectory($dir);
                $original = Yii::getAlias('@webroot/images/uploads/source/') . $file;
                try {
                    if (filesize($original) < 10000000) {
                        Image::thumbnail($original, $width, $height)->save($img, ['quality' => 100]);
                    }
                    $url = true;
                } catch (ErrorException $e) {
                    $url = false;
                }
            }
        }
        return $url ? '/images/uploads/' . $width . 'x' . $height . '/' . $file : '/images/anonymous.png';
    }
}
?>
<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "consultation_office".
 *
 * @property integer $id
 * @property string $name
 * @property string $city
 * @property string $phone
 * @property string $time
 * @property integer $day
 * @property string $text
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class ConsultationOffice extends \yii\db\ActiveRecord
{
    public $consultation_time = [
        '10-00' => '10-00',
        '11-00' => '11-00',
        '12-00' => '12-00',
        '13-00' => '13-00',
        '14-00' => '14-00',
        '15-00' => '15-00',
        '16-00' => '16-00',
        '17-00' => '17-00',
        '18-00' => '18-00',
        '19-00' => '19-00'
    ];
    
    public $consultation_day = [1 => 'Сегодня', 2 => 'Завтра'];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consultation_office';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'time', 'day'], 'required'],
            [['day', 'status', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['city'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
            [['time'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'city' => 'Город',
            'phone' => 'Телефон',
            'time' => 'Время',
            'day' => 'День',
            'text' => 'Вопрос',
            'status' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен'
        ];
    }
}

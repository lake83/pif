<?php
namespace app\models;

use Yii;
use app\models\User;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $verify_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'match', 'pattern' => '/^[a-zA-Z0-9]\w+$/'],
            ['username', 'string', 'min' => 3, 'max' => 25],
            ['username', 'trim'],
            ['username', 'unique', 'targetClass' => User::className(), 'message' => 'Этот логин уже занят.'],

            ['email', 'required'],
            ['email', 'email'],
            [['email'], 'string', 'max' => 100],
            ['email', 'trim'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'Этот E-mail уже занят.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            
            ['verify_password', 'required'],
            ['verify_password', 'compare', 'compareAttribute' => 'password'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'verify_password' => 'Пароль повторно',
            'email' => 'E-mail'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate())
        {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->status = 10;
            $user->is_active = $user->activeAfterRegistration ? 1 : 0;
            $user->generateAuthKey();
            if ($user->save()) {
                if(!$user->activeAfterRegistration) {
                    Yii::$app->mailer->compose(['html' => 'accountActivation-html', 'text' => 'accountActivation-text'], ['user' => $user])
                        ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name])
                        ->setTo($this->email)
                        ->setSubject('Активация аккаунта на '. Yii::$app->name)
                        ->send();
                }
                return $user;
            }
        }
        return null;
    }
}
<?php

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

$activationLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/accaunt-activation', 'token' => $user->auth_key]);
?>
Здравствуйте <?= $user->username ?>,

Перейдите по следующей ссылке для активации аккаунта:

<?= $activationLink ?>
<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

$activationLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/accaunt-activation', 'token' => $user->auth_key]);
?>
<div class="password-reset">
    <p>Здравствуйте <?= Html::encode($user->username) ?>,</p>

    <p>Перейдите по следующей ссылке для активации аккаунта:</p>

    <p><?= Html::a(Html::encode($activationLink), $activationLink) ?></p>
</div>
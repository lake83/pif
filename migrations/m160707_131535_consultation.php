<?php

use yii\db\Migration;

class m160707_131535_consultation extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql')
        {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('consultation_office', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'city' => $this->string(100)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'time' => $this->string(5)->notNull(),
            'day' => $this->boolean()->comment('1-сегодня,2-завтра'),
            'text' => $this->string()->notNull(),
            'status' => $this->boolean()->defaultValue(1)->comment('1-новое,2-обработано'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ], $tableOptions);
        
        $this->createTable('consultation_phone', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'city' => $this->string(100)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'status' => $this->boolean()->defaultValue(1)->comment('1-новое,2-обработано'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('consultation_office');
        $this->dropTable('consultation_phone');
    }
}

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\News;
use app\models\NewsSearch;
use app\models\ConsultationOffice;
use app\models\ConsultationPhone;
use app\models\SignupForm;
use yii\web\BadRequestHttpException;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $modelOffice = new ConsultationOffice;
        $modelPhone = new ConsultationPhone;
        
        if($modelOffice->load(Yii::$app->request->post()) && $modelOffice->save()) {
            Yii::$app->session->setFlash('success', 'Ваша заявка сохранена.');
            return $this->refresh();
        }
        if($modelPhone->load(Yii::$app->request->post()) && $modelPhone->save()) {
            Yii::$app->session->setFlash('success', 'Ваша заявка сохранена.');
            return $this->refresh();
        }
        return $this->render('index', [
            'modelOffice' => $modelOffice,
            'modelPhone' => $modelPhone,
        ]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['admin/site/index']);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionRegistration()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if($user->activeAfterRegistration) {
                    if (Yii::$app->user->login($user))
                        return $this->goHome();
                } else {
                    Yii::$app->session->setFlash('success', 'Инструкции по завершению регистрации отправлены на Ваш e-mail.');
                    return $this->goHome();
                }
            }
        }
        
        return $this->render('registration', [
            'model' => $model
        ]);
    }
    
    public function actionAccauntActivation($token)
    {
        if (empty($token) || !is_string($token)) {
            throw new BadRequestHttpException('Токен не может быть пустым.');
        } else {
            $model = User::findOne(['auth_key' => $token]);
        }
        if (!$model || !$model->validateAuthKey($token)) {
            throw new BadRequestHttpException('Неправильный токен.');
        } elseif ($model->is_active > 0) {
            throw new BadRequestHttpException('Ваш аккаунт уже активирован.');
        } else {
            $model->is_active = 1;
            if($model->save()) {
                Yii::$app->session->setFlash('success', 'Ваш аккаунт активирован.');
                return $this->goHome();
            }
        }
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            
            $admin = User::findOne(['status' => User::ROLE_ADMIN]);
            if (empty($admin) || empty($admin->email)) {
                throw new BadRequestHttpException('Не задан E-mail администратора.');
            } 
            $model->contact($admin->email);
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionNews()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('news', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    public function actionArticle($slug)
    {
        $model = News::find()->where(['slug' => $slug])->asArray()->one();
        
        $this->isModel($model);
        
        return $this->render('article', [
            'model' => $model
        ]);
    }
    
    private function isModel($model)
    {
        if (!$model) {
            throw new BadRequestHttpException('Страница не найдена.');
        } 
    }
}

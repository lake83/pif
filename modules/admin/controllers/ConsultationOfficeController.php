<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers\AdminController;

/**
 * ConsultationOfficeController implements the CRUD actions for ConsultationOffice model.
 */
class ConsultationOfficeController extends AdminController
{
    public function actions()
    {
        return [
            'index' => [
                'class' => $this->actionsPath.'Index',
                'search' => $this->modelPath.'ConsultationOfficeSearch'
            ],
            'update' => [
                'class' => $this->actionsPath.'Update',
                'model' => $this->modelPath.'ConsultationOffice'
            ],
            'delete' => [
                'class' => $this->actionsPath.'Delete',
                'model' => $this->modelPath.'ConsultationOffice'
            ]
        ];
    }
}
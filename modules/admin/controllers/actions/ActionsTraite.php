<?php
namespace app\modules\admin\controllers\actions;

use Yii;

trait ActionsTraite
{
    /**
     * Аякс валидация, сохранение и вывод для экшенов Create и Update
     * @param object $model экземпляр модели
     * @param string $message сообщение при успешном сохранении
     * @param string $view вид  
     * @return mixed
     */
    public function actionBody($model, $message, $view)
    {
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', $message);
            return $this->controller->redirect(['index']);
        } else {
            return Yii::$app->request->isAjax ? $this->controller->renderAjax($view, [
                'model' => $model
            ]) : $this->controller->render($view, [
                'model' => $model
            ]);
        }
    }
}
?>
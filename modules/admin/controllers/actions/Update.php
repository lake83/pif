<?php 
namespace app\modules\admin\controllers\actions;

use Yii;
use yii\web\NotFoundHttpException;

class Update extends \yii\base\Action
{
    use ActionsTraite;
    
    public $model;
    
    public function run()
    {
        $model = $this->model;
        $model = $model::findOne(Yii::$app->request->getQueryParam('id'));
        
        if ($model == null) 
            throw new NotFoundHttpException(Yii::t('app', 'Страница не найдена.'));
            
        return $this->actionBody($model, Yii::t('app', 'Изменения сохранены.'), 'update');
    }
} 
?>
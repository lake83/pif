<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers\AdminController;

/**
 * ConsultationPhoneController implements the CRUD actions for ConsultationPhone model.
 */
class ConsultationPhoneController extends AdminController
{
    public function actions()
    {
        return [
            'index' => [
                'class' => $this->actionsPath.'Index',
                'search' => $this->modelPath.'ConsultationPhoneSearch'
            ],
            'update' => [
                'class' => $this->actionsPath.'Update',
                'model' => $this->modelPath.'ConsultationPhone'
            ],
            'delete' => [
                'class' => $this->actionsPath.'Delete',
                'model' => $this->modelPath.'ConsultationPhone'
            ]
        ];
    }
}

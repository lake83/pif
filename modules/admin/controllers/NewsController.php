<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers\AdminController;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends AdminController
{
    public function actions()
    {
        return [
            'index' => [
                'class' => $this->actionsPath.'Index',
                'search' => $this->modelPath.'NewsSearch'
            ],
            'create' => [
                'class' => $this->actionsPath.'Create',
                'model' => $this->modelPath.'News'
            ],
            'update' => [
                'class' => $this->actionsPath.'Update',
                'model' => $this->modelPath.'News'
            ],
            'delete' => [
                'class' => $this->actionsPath.'Delete',
                'model' => $this->modelPath.'News'
            ]
        ];
    }
}

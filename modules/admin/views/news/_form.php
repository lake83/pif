<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\components\FilemanagerInput;
use app\components\RedactorTinymce;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\bootstrap\ActiveForm */

$form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($model, 'title')?>

    <?= $form->field($model, 'seo_key')?>

    <?= $form->field($model, 'seo_description') ?>
    
    <?= $form->field($model, 'image')->widget(FilemanagerInput::className())?>
    
    <?= $form->field($model, 'text')->widget(RedactorTinymce::className())?>

    <?= $form->field($model, 'is_active')->checkbox()?>

    <div class="box-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

<?php ActiveForm::end(); ?>
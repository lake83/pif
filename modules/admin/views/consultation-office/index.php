<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ConsultationOfficeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Консультации в офисе'; ?>

<h1><?= Html::encode($this->title) ?></h1>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{items}{pager}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'city',
            'phone',
            'time',
            [
                'attribute' => 'day',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'day',
                    $searchModel->consultation_day,
                    ['class' => 'form-control', 'prompt' => '- выбрать -']
                ),
                'value' => function ($model, $index, $widget) {
                    return $model->day == 1 ? 'Сегодня' : 'Завтра';}
            ],
            'text',
            [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    [1 => 'Новое', 2 => 'Обработано'],
                    ['class' => 'form-control', 'prompt' => '- выбрать -']
                ),
                'value' => function ($model, $index, $widget) {
                    return $model->status == 1 ? 'Новое' : 'Обработано';}
            ],
            [
                'attribute' => 'created_at',
                'format' => 'date',
                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'options' => ['class' => 'form-control'],               
                    'attribute'=>'created_at',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ])
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'options' => ['width' => '50px']
            ]
        ],
]); ?>

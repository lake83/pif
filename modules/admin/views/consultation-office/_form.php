<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ConsultationPhone */
/* @var $form yii\bootstrap\ActiveForm */

$listOptions = ['class' => 'form-control', 'prompt' => '- выбрать -'];

$form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '999-999-9999']) ?>
    
    <?= $form->field($model, 'time')->dropDownList($model->consultation_time, $listOptions) ?>
    
    <?= $form->field($model, 'day')->dropDownList($model->consultation_day, $listOptions) ?>
    
    <?= $form->field($model, 'text')->textArea(['rows' => '6']) ?>

    <?= $form->field($model, 'status')->dropDownList([1 => 'Новое', 2 => 'Обработано'], $listOptions) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

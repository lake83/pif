<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Настройки';
?>
<div class="site-index">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
    
        <div class="col-lg-6">
        
            <?= $form->field($model, 'email') ?>
            
            <?= $form->field($model, 'username') ?>
            
            <?= $form->field($model, 'new_password')->passwordInput() ?>
        
        </div>
    
    </div>
    
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
    
</div>